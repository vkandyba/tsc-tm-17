package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.model.Command;

import java.util.Collection;

public interface ICommandRepository {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    Collection<String> getListCommandName();

    Collection<String> getListCommandArg();

    void add(AbstractCommand command);

}
